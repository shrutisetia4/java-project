<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tf" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Arithmetic Input</title>
	</head>
	
	<body>
		
		<!-- Header and Footer tags demonstrate the use of html5 -->
		<header>
		   Arithmetic Input
		</header>
		
		<!-- Below is custom tag from .tag file -->
		<P> <tf:customExpTag/> </p>
		
		<!-- Below tag demonstrates the use of spring-tags -->
		<form:form action="evaluate" method="post">
			<form:input type="text" path="arithmeticExp"/><br> 
			<input type="submit" value="Submit">
		</form:form>
		
		<footer>
		  Opiant Technologies
		</footer>
		
	</body>
</html>