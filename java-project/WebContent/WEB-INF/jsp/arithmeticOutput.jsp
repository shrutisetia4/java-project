<!-- Below tag demonstrates the use of jsp tags -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Arithmetic Expression Evaluation</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<style>
         #rcorners1 {
            border-radius: 25px;
            background: #8AC007;
            padding: 20px;
            width: 200px;
            height: 150px;
         }
         </style>
         
         <!-- Below tag demonstrates the use of juery -->
         <script>
         window.onload=function(){
             $('.myClass').css({'background-color': 'yellow'}); 
        }
         </script>
	</head>
	
	<body>
	    
	    <p class="myClass">The value of arithmetic expression is :</p>

	    <!-- rounded corners demonstrate the use of css3 -->
	    <p id = "rcorners1">${value}</p>

	</body>
</html>