package com.opiant.arithmetic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.opiant.arithmetic.Calculator;
import com.opiant.arithmetic.model.ExpDetail;
import com.opiant.arithmetic.serv.ArithmeticService;


import java.util.logging.Logger;

@Controller
public class ArithmeticController {

	private static final Logger LOGGER = Logger.getLogger(ArithmeticController.class.getName());
	
	 @Autowired 
	 ArithmeticService service;
	 
	
	/*
	 @RequestMapping(value = "/arithmeticInput", method = RequestMethod.GET)
	public ModelAndView arExpression() {
	      return new ModelAndView("arithmeticInput", "command", new ArExpression());
	   }
	
	 @RequestMapping(value = "/evaluate", method = RequestMethod.POST)
	 public String  evaluate(@ModelAttribute("arExpression")ArExpression arExpression, ModelMap model) {
		 
		 Double value = Calculator.calc(arExpression.getArithmeticExp());
		 LOGGER.info("value: "+value);
		 model.addAttribute("value", value);
		 
		 ///////
		 
		 ExpDetail expDetail = new ExpDetail();
		 expDetail.setId(1);
		 expDetail.setArithmeticExp("3");
		 expDetail.setResult("3");
         service.save(expDetail);
         LOGGER.info("hiiiii::: "+ expDetail.getId());
		 //////
		 
		 //////
		 
         List<ExpDetail> exp = service.getAll();
		 for(ExpDetail expr:exp) {
			 LOGGER.info("expppp: "+expr.getArithmeticExp());
		 }
		 
		 //////
		  
		 
	     
	     return "arithmeticOutput";
	     
	  }
	  */
	 
	 @RequestMapping(value = "/arithmeticInput", method = RequestMethod.GET)
		public ModelAndView arExpression() {
		      return new ModelAndView("arithmeticInput", "command", new ExpDetail());
		   }
		
		 @RequestMapping(value = "/evaluate", method = RequestMethod.POST)
		 public String  evaluate(@ModelAttribute("arExpression")ExpDetail expDetail, ModelMap model) {
			 
			 Double value = Calculator.calc(expDetail.getArithmeticExp());
			 LOGGER.info("value....: "+value);
			 model.addAttribute("value", value);
			 
			 //save in h2 db
			 expDetail.setResult(value);
			 service.save(expDetail);
			 LOGGER.info("expDetail id::: "+ expDetail.getId());
			 
			 return "arithmeticOutput";
		     
		  }
	
}
