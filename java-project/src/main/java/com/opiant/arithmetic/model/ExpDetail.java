package com.opiant.arithmetic.model;

import java.io.Serializable;

import javax.persistence.Column;  
import javax.persistence.Entity;  
import javax.persistence.GeneratedValue;  
import javax.persistence.GenerationType;  
import javax.persistence.Id;  
import javax.persistence.Table; 

@Table(name="exp_detail")  
@Entity  
public class ExpDetail implements Serializable{
	  private static final long serialVersionUID = 1L;
	  
	  private Integer id;  
	  private String arithmeticExp;  
	  private Double result; 
	  
	  public ExpDetail() {
	    }
	  
	/*
	 * public ExpDetail(Integer id, String exp, String result) { super(); this.id =
	 * id; this.exp = exp; this.result = result; }
	 */
	
	  @Id  
	  @GeneratedValue(strategy=GenerationType.IDENTITY)  
	    public Integer getId()  
	    {  
	        return id;  
	    }  
	    public void setId(Integer id)  
	    {  
	        this.id = id;  
	    }
	    
	    @Column(name="arithmeticExp") 
	    public String getArithmeticExp() {
			return arithmeticExp;
		}
	
	   public void setArithmeticExp(String arithmeticExp) { 
		   this.arithmeticExp = arithmeticExp; 
		   }
	 
		
		@Column(name="result") 
		public Double getResult() {
			return result;
		}
	
	    public void setResult(Double result) {
	    	this.result = result;
	    	}
	
	    
	    @Override
	    public String toString() {
	        return "ExpDetail [id=" + id + ", arithmeticExp=" + arithmeticExp + ", result=" + result + "]";
	    }
	    
	    
}
