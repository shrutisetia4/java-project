package com.opiant.arithmetic.repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.opiant.arithmetic.model.ExpDetail;

@Repository
public interface ArExRepository extends JpaRepository<ExpDetail, Integer>{

}
