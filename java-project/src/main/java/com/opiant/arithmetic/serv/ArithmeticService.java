package com.opiant.arithmetic.serv;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.opiant.arithmetic.model.ExpDetail;
import com.opiant.arithmetic.repository.ArExRepository;

@Service
public class ArithmeticService {
 
    // @Autowired annotation provides the automatic dependency injection.
    @Autowired
    ArExRepository repository;
 
    public void save(final ExpDetail expDetail) {
        repository.save(expDetail);
    }
 
    public List<ExpDetail> getAll() {
        final List<ExpDetail> exp = new ArrayList<>();
        repository.findAll().forEach(expr -> exp.add(expr));
        return exp;
    }
}

